<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Categoria extends CI_Controller {
	function __construct() { /* 	CREADO PARA USAR EL FORM  	*/
		parent::__construct();
		$this->load->helper('form');
	}

	public function index()	{
		$datos['segmento'] = $this->uri->segment(3);
		$this->load->view('backend/head');
		$this->load->view('backend/navigation');
		if (!$datos['segmento']) {
			$datos['categorias'] = $this->Categoria_model->obtenerCategorias();
		}
		else{
			$datos['categorias'] = $this->Categoria_model->obtenerCategoria($datos['segmento']);
		}
		$this->load->view('backend/categorias/listar', $datos);
		$this->load->view('backend/footer');
	}

	public function nueva()	{
		$this->load->view('backend/head');
		$this->load->view('backend/navigation');
		$this->load->view('backend/categorias/registro');
		$this->load->view('backend/footer');
	}

	public function recibirDatos()	{ /* FALTA AÑADIR IMAGEN */
		$datos = array(
			'nombre' => $this->input->post('nombre'), 
			'color' => $this->input->post('color') 
		);
	}

}