<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	public function index()
	{
		// $this->load->view('frontend/head');
		// $this->load->view('frontend/navigation');
		// $this->load->view('welcome_message');
		// $this->load->view('frontend/footer');

		$this->load->view('backend/head');
		$this->load->view('backend/navigation');
		$this->load->view('backend/content');
		$this->load->view('backend/footer');
	}
}
