		<h2>Registro de nueva Categoría</h2>
		<?= form_open("/categoria/recibirdatos") ?>
		<?php
			$nombre = array(
				'name' => 'nombre',
				'placeholder' => 'Escribe nueva categoria' ,
			);

			$color = array(
				'color' => 'color',
				'placeholder' => 'Selecciona el color' ,
				'type' => 'color',
			);

			$imagen = array(
				'image' => 'imagen',
				'placeholder' => 'Dirección de la imagen',
				'type' => 'url'
			);
		?>

		<?= form_label('Nombre:', 'nombre') ?>
		<?= form_input($nombre) ?>
		<br>
		<?= form_label('Color:', 'color') ?>
		<?= form_input($color) ?>
		<br>
		<?= form_label('Imagen:', 'imagen') ?>
		<?= form_input($imagen) ?>
		<br>
		<?= form_submit('','Registrar') ?>
		<?= form_reset('','Limpiar campos') ?>
		<?= form_close() ?>