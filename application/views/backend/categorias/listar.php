    <!-- intento 2 -->
    <div>
      <h2>Categorías</h2>
    </div>
    <div>
      <?php if (!$segmento):?>
        <table>
          <thead>
            <tr>
              <th>#</th>
              <th>Nombre</th>
              <th>Color</th>
            </tr>
          </thead>
          <tbody>
            <?php if ($categorias): ?>
            <?php foreach ($categorias->result() as $categoria): ?>
            <tr>
              <th><a href="<?= base_url()?>categoria/index/<?=$categoria->id_categoria?>"><?= $categoria->id_categoria ?></a></th>
              <td><a href="<?= base_url()?>categoria/index/<?=$categoria->id_categoria?>"><?= $categoria->nombre ?></a></td>
              <td><input type="color" name="color" value="<?= $categoria->color ?>" disabled></td>
            </tr>
            <?php endforeach; ?>
            <?php else: ?>
              <p>Error en la aplicación</p>
            <?php endif; ?>
          </tbody>
        </table>
      <?php else: ?>
        <?php foreach ($categorias->result() as $categoria): ?>
          <img src="<?= $categoria->imagen ?>" alt="Imagen de <?= $categoria->nombre ?>">
          <h2><?= $categoria->nombre ?></h2>
          <input type="color" name="color" value="<?= $categoria->color ?>" disabled>
        <?php endforeach; ?>
      <?php endif; ?>
    </div>
