  <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top" id="mainNav">
    <a class="navbar-brand" href="index.html">Virtual Library System</a>
    <!-- <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="menuModal" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
      </button> -->
      <div class="" id="btn-menu">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item">
            <a class="nav-link" data-toggle="modal" data-target="#menuModal">
              <span class="navbar-toggler-icon"></span></a>
          </li>
        </ul>
      </div>
<!-- 
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item">
            <a class="nav-link" data-toggle="modal" data-target="#menuModal">
              <i class="fa fa-bars"></i></a>
          </li>
        </ul>
      </div> -->
  </nav>        

    <div class="modal fade" id="menuModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog  modal-lg" role="document">
        <div class="modal-content">
          <!-- <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">¿Desea salir?</h5>
            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
          </div> -->
          <div class="modal-body">
            <div class="row">
              <div class="col-xl-6 col-sm-6 mb-3">
                <div class="card text-white bg-primary o-hidden h-100">
                  <div class="card-body">
                    <div class="card-body-icon">
                      <i class="fa fa-book"></i>
                    </div>
                    <div class="mr-5 text-right no-margin"><strong>26</strong> Libros</div>
                  </div>
                  <a class="card-footer text-white clearfix small z-1" href="#">
                    <span class="float-right">
                      <i class="fa fa-angle-right"></i>
                    </span>
                    <span class="float-right">View Details</span>
                  </a>
                </div>
              </div>

              <div class="col-xl-6 col-sm-6 mb-3">
                <div class="card text-white bg-info o-hidden h-100">
                  <div class="card-body">
                    <div class="card-body-icon">
                      <i class="fa fa-pencil-square"></i>
                    </div>
                    <div class="mr-5 text-right no-margin"><strong>11</strong> Autores</div>
                  </div>
                  <a class="card-footer text-white clearfix small z-1" href="#">
                    <span class="float-right">
                      <i class="fa fa-angle-right"></i>
                    </span>
                    <span class="float-right">View Details  </span>
                  </a>
                </div>
              </div>
            </div>

            <div class="row">
              <div class="col-xl-6 col-sm-6 mb-3">
                <div class="card text-white bg-success o-hidden h-100">
                  <div class="card-body">
                    <div class="card-body-icon">
                      <i class="fa fa-fw fa-list"></i>
                    </div>
                    <div class="mr-5 text-right no-margin"><strong>123</strong> Categorías</div>
                  </div>
                  <a class="card-footer text-white clearfix small z-1" href="#">
                    <span class="float-right">
                      <i class="fa fa-angle-right"></i>
                    </span>
                    <span class="float-right">View Details</span>
                  </a>
                </div>
              </div>

              <div class="col-xl-6 col-sm-6 mb-3">
                <div class="card text-white bg-danger o-hidden h-100">
                  <div class="card-body">
                    <div class="card-body-icon">
                      <i class="fa fa-users"></i>
                    </div>
                    <div class="mr-5 text-right no-margin"><strong>13</strong> Usuarios</div>
                  </div>
                  <a class="card-footer text-white clearfix small z-1" href="#">
                    <span class="float-right">
                      <i class="fa fa-angle-right"></i>
                    </span>
                    <span class="float-right">View Details</span>
                  </a>
                </div>
              </div>
            </div>

          </div>
          <div class="modal-footer">
            <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancelar</button>
            <a class="btn btn-primary" href="login.html">Salir</a>
          </div>
        </div>
      </div>
    </div>