
    <!-- Footer -->
    <footer class="padding-footer bg-dark">
      <div class="container">
        <p class="m-0 text-center text-white">Copyright &copy; Your Website 2017</p>
      </div>
      <!-- /.container -->
    </footer>

    <!-- Bootstrap core JavaScript -->
    <script src="<?= base_url() ?>template/frontend/vendor/jquery/jquery.min.js"></script>
    <script src="<?= base_url() ?>template/frontend/vendor/popper/popper.min.js"></script>
    <script src="<?= base_url() ?>template/frontend/vendor/bootstrap/js/bootstrap.min.js"></script>

  </body>

</html>
