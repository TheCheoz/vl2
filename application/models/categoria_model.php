<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Categoria_model extends CI_Model {
	public function __construct(){
		parent::__construct();
		$this->load->database();
	}
	public function obtenerCategorias()	{
		$query = $this->db->get('categoria');
		if ($query->num_rows() > 0) {
			return $query;
		}
		else return false;
	}

	public function obtenerCategoria($id)	{
		$this->db->where('id_categoria',$id);
		$query = $this->db->get('categoria');
		if ($query->num_rows() > 0) {
			return $query;
		}
		else return false;
	}
}
?>